import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContasRoutingModule } from './contas-routing.module';

import { ContasPageModule } from './contas/contas.module';
import { RelatorioPageModule } from './relatorio/relatorio.module';
import { CadastroPageModule } from './cadastro/cadastro.module';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ContasRoutingModule,
    ContasPageModule,
    RelatorioPageModule,
    CadastroPageModule,
    IonicModule,
  ],
})
export class ContasModule {}
