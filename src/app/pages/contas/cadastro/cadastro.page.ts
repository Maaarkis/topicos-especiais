import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { DateHelper } from 'src/app/shared/helpers/Date.helper';
import { ToastService } from 'src/app/shared/toast.service';
import { ContasService } from '../services/contas.service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private nav: NavController,
    private datePipe: DatePipe,
    private contaService: ContasService,
    private toastService: ToastService
  ) {}

  ngOnInit() {
    this.form = this.createForm();
  }

  public onSubmit(): void {
    let { value } = this.form;
    const date = this.form.get('data').value;
    value = { ...value, ...DateHelper.breakDate(date) };

    this.contaService
      .record(value)
      .then(() => {
        this.toastService.showToast({
          message: 'Conta cadastrada com sucesso!',
          duration: 3000,
          position: 'bottom',
        });
        this.nav.navigateBack(`/contas/${value.tipo}`);
      })
      .catch(() =>
        this.toastService.showToast({
          message: 'Erro ao contrato!',
          duration: 3000,
          position: 'bottom',
        })
      );
  }

  private createForm(): FormGroup {
    return this.fb.group({
      nome: ['', [Validators.required, Validators.minLength(3)]],
      descricao: ['', [Validators.required, Validators.minLength(3)]],
      valor: ['', [Validators.required, Validators.min(1)]],
      data: [
        this.datePipe.transform(Date.now(), 'yyyy-MM-dd'),
        [Validators.required],
      ],
      tipo: ['', [Validators.required]],
    });
  }
}
