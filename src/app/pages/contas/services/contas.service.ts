import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DateHelper } from 'src/app/shared/helpers/Date.helper';
import {
  Conta,
  ContaRegister,
} from 'src/app/shared/interfaces/conta.interface';

@Injectable({
  providedIn: 'root',
})
export class ContasService {
  private nameCollection = 'contas';

  constructor(private fireStore: AngularFirestore) {}

  public record(value: ContaRegister): Promise<void> {
    value.id = this.fireStore.createId();
    return this.fireStore
      .collection(this.nameCollection)
      .doc(value.id)
      .set(value);
  }

  public getContas(tipo: string = 'despesa'): Observable<Conta[]> {
    const contas = this.fireStore.collection<Conta>(
      this.nameCollection,
      (ref) => ref.where('tipo', '==', tipo)
    );
    return contas.valueChanges();
  }

  public edit(conta: Conta): void {
    const { id } = conta;

    this.fireStore
      .doc(`${this.nameCollection}/${id}`)
      .update(conta)
      .catch((error) => console.log(error));
  }

  public delete(id: string): void {
    console.log(id);

    this.fireStore
      .doc(`${this.nameCollection}/${id}`)
      .delete()
      .catch((error) => console.log(error));
  }

  public totalPor(tipo: string, searchDate: string) {
    const { mes, ano } = DateHelper.breakDate(searchDate);
    const contas = this.fireStore.collection<Conta>(
      this.nameCollection,
      (ref) =>
        ref
          .where('tipo', '==', tipo)
          .where('mes', '==', mes)
          .where('ano', '==', ano)
    );
    console.log(contas);

    return contas.get().pipe(
      map((snap) => {
        let valorTotal = 0;
        let totalConta = 0;

        snap.docs.map((doc) => {
          const conta = doc.data();

          valorTotal += conta.valor;
          totalConta++;
        });
        return { valorTotal, totalConta };
      })
    );
  }
}
