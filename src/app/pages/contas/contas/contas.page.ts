import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Conta } from 'src/app/shared/interfaces/conta.interface';
import { ContasService } from '../services/contas.service';

@Component({
  selector: 'app-contas',
  templateUrl: './contas.page.html',
  styleUrls: ['./contas.page.scss'],
})
export class ContasPage implements OnInit {
  contas: Conta[] = [];
  tipo: string;

  constructor(
    private contaService: ContasService,
    private alertController: AlertController,
    private datePipe: DatePipe,
    private router: Router
  ) {}

  ngOnInit() {
    const { url } = this.router;
    this.tipo = url.split('/').pop();

    this.contaService.getContas(this.tipo).subscribe((res: Conta[]) => {
      this.contas = res;
    });
  }

  public async delete(conta: Conta): Promise<void> {
    const alert = await this.alertController.create({
      header: 'Excluir conta',
      message: `Deseja excluir a conta ${conta.nome}?`,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Excluir',
          handler: () => {
            const { id } = conta;
            this.contaService.delete(id);
          },
        },
      ],
    });

    alert.present();
  }

  public async edit(conta: Conta): Promise<void> {
    const alert = await this.alertController.create({
      header: 'Editar conta',
      message: `Deseja editar a conta ${conta.nome}?`,
      inputs: [
        {
          name: 'nome',
          type: 'text',
          value: conta.nome,
        },
        {
          name: 'descricao',
          type: 'text',
          value: conta.descricao,
        },
        {
          name: 'valor',
          type: 'number',
          value: conta.valor,
        },
        {
          name: 'data',
          type: 'date',
          value: this.datePipe.transform(conta.data, 'yyyy-MM-dd'),
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Enviar',
          handler: (contaEditada) => {
            this.contaService.edit({ ...conta, ...contaEditada });
          },
        },
      ],
    });

    alert.present();
  }
}
