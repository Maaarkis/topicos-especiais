import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: 'pagar',
      },
      {
        path: 'pagar',
        loadChildren: () =>
          import('./contas/contas.module').then((m) => m.ContasPageModule),
      },
      {
        path: 'receber',
        loadChildren: () =>
          import('./contas/contas.module').then((m) => m.ContasPageModule),
      },
      {
        path: 'relatorio',
        loadChildren: () =>
          import('./relatorio/relatorio.module').then(
            (m) => m.RelatorioPageModule
          ),
      },
      {
        path: 'cadastro',
        loadChildren: () =>
          import('./cadastro/cadastro.module').then(
            (m) => m.CadastroPageModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContasRoutingModule {}
