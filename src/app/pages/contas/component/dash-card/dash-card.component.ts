import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-dash-card',
  templateUrl: './dash-card.component.html',
  styleUrls: ['./dash-card.component.scss'],
})
export class DashCardComponent implements OnInit {
  @Input() title = '';
  @Input() icon = '';
  @Input() quantidade = 0;
  @Input() value = 0;
  @Input() color = '';

  constructor() {}

  ngOnInit() {}
}
