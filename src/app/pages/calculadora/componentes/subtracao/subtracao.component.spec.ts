import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { IonicModule } from '@ionic/angular';

import { SubtracaoComponent } from './subtracao.component';

describe('SubtracaoComponent', () => {
  let component: SubtracaoComponent;
  let fixture: ComponentFixture<SubtracaoComponent>;
  let view;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [SubtracaoComponent],
        imports: [IonicModule.forRoot(), FormsModule, ReactiveFormsModule],
      }).compileComponents();

      fixture = TestBed.createComponent(SubtracaoComponent);
      component = fixture.componentInstance;
      view = fixture.nativeElement;
      fixture.detectChanges();
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('deve estar com o botão desabilitado', () => {
    expect(component.form.invalid).toBeTrue();
  });

  it('Multiplicar com clique no botão', () => {
    component.form.controls.numero1.setValue(24);
    component.form.controls.numero2.setValue(10);
    const resultadoEsperado = (24 - 10).toString();

    const operacao = fixture.debugElement.query(By.css('#operacaoSubstracao'));
    operacao.triggerEventHandler('click', null);
    fixture.detectChanges();

    const resultado = view.querySelector('#resultadoSubstracao');
    expect(resultado.textContent).toBe(resultadoEsperado);
  });
});
