import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CalculadoraService } from '../../calculadora.service';

@Component({
  selector: 'app-subtracao',
  templateUrl: './subtracao.component.html',
})
export class SubtracaoComponent implements OnInit {
  form: FormGroup;
  resultado = '0';

  constructor(
    private fb: FormBuilder,
    private calculadoraService: CalculadoraService
  ) {}

  ngOnInit() {
    this.createForm();
  }

  createForm(): void {
    this.form = this.fb.group({
      numero1: ['', [Validators.required]],
      numero2: ['', [Validators.required]],
    });
  }

  public subtrair(): void {
    const { numero1, numero2 } = this.form.value;

    this.resultado = this.calculadoraService.subtrair(numero1, numero2);
  }
}
