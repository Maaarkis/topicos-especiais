import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CalculadoraService } from '../../calculadora.service';

@Component({
  selector: 'app-multiplicacao',
  templateUrl: './multiplicacao.component.html',
})
export class MultiplicacaoComponent implements OnInit {
  form: FormGroup;
  resultado = '0';

  constructor(
    private fb: FormBuilder,
    private calculadoraService: CalculadoraService
  ) {}

  ngOnInit() {
    this.createForm();
  }

  createForm(): void {
    this.form = this.fb.group({
      numero1: ['', [Validators.required]],
      numero2: ['', [Validators.required]],
    });
  }

  public multiplicar(): void {
    const { numero1, numero2 } = this.form.value;

    this.resultado = this.calculadoraService.multiplicar(numero1, numero2);
  }
}
