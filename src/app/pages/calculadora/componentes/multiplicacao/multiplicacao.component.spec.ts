import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { IonicModule } from '@ionic/angular';

import { MultiplicacaoComponent } from './multiplicacao.component';

describe('MultiplicacaoComponent', () => {
  let component: MultiplicacaoComponent;
  let fixture: ComponentFixture<MultiplicacaoComponent>;
  let view;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [MultiplicacaoComponent],
        imports: [IonicModule.forRoot(), FormsModule, ReactiveFormsModule],
      }).compileComponents();

      fixture = TestBed.createComponent(MultiplicacaoComponent);
      component = fixture.componentInstance;
      view = fixture.nativeElement;
      fixture.detectChanges();
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('deve estar com o botão desabilitado', () => {
    expect(component.form.invalid).toBeTrue();
  });

  it('Multiplicar com clique no botão', () => {
    component.form.controls.numero1.setValue(24);
    component.form.controls.numero2.setValue(10);
    const resultadoEsperado = (10 * 24).toString();

    const operacao = fixture.debugElement.query(
      By.css('#operacaoMultiplicacao')
    );
    operacao.triggerEventHandler('click', null);
    fixture.detectChanges();

    const resultado = view.querySelector('#resultadoMultiplicacao');
    expect(resultado.textContent).toBe(resultadoEsperado);
  });
});
