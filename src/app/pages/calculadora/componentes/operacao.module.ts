import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SomaComponent } from './soma/soma.component';
import { IonicModule } from '@ionic/angular';
import { SubtracaoComponent } from './subtracao/subtracao.component';
import { DivisaoComponent } from './divisao/divisao.component';
import { MultiplicacaoComponent } from './multiplicacao/multiplicacao.component';

@NgModule({
  declarations: [
    SomaComponent,
    SubtracaoComponent,
    DivisaoComponent,
    MultiplicacaoComponent,
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
  ],
  exports: [
    SomaComponent,
    SubtracaoComponent,
    DivisaoComponent,
    MultiplicacaoComponent,
  ],
})
export class OperacaoModule {}
