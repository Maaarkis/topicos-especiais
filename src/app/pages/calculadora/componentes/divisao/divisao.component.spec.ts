import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { By } from '@angular/platform-browser';
import { DivisaoComponent } from './divisao.component';

describe('DivisaoComponent', () => {
  let component: DivisaoComponent;
  let fixture: ComponentFixture<DivisaoComponent>;
  let view;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [DivisaoComponent],
        imports: [IonicModule.forRoot(), FormsModule, ReactiveFormsModule],
      }).compileComponents();

      fixture = TestBed.createComponent(DivisaoComponent);
      component = fixture.componentInstance;
      view = fixture.nativeElement;
      fixture.detectChanges();
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('deve estar com o botão desabilitado', () => {
    expect(component.form.invalid).toBeTrue();
  });

  it('dividi com clique no botão', () => {
    component.form.controls.divisor.setValue(24);
    component.form.controls.dividendo.setValue(10);
    const resultadoEsperado = (10 / 24).toString();

    const operacao = fixture.debugElement.query(By.css('#operacaoDividir'));
    operacao.triggerEventHandler('click', null);
    fixture.detectChanges();

    const resultado = view.querySelector('#resultadoDivisao');
    expect(resultado.textContent).toBe(resultadoEsperado);
  });
});
