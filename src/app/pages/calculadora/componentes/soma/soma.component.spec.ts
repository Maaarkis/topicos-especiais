import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { IonicModule } from '@ionic/angular';

import { SomaComponent } from './soma.component';

describe('SomaComponent', () => {
  let component: SomaComponent;
  let fixture: ComponentFixture<SomaComponent>;
  let view;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [SomaComponent],
        imports: [IonicModule.forRoot(), FormsModule, ReactiveFormsModule],
      }).compileComponents();

      fixture = TestBed.createComponent(SomaComponent);
      component = fixture.componentInstance;
      view = fixture.nativeElement;
      fixture.detectChanges();
    })
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('deve estar com o botão desabilitado', () => {
    expect(component.form.invalid).toBeTrue();
  });

  it('Soma com clique no botão', () => {
    component.form.controls.numero1.setValue(24);
    component.form.controls.numero2.setValue(10);
    const resultadoEsperado = (10 + 24).toString();

    const operacao = fixture.debugElement.query(By.css('#operacaoSoma'));
    operacao.triggerEventHandler('click', null);
    fixture.detectChanges();

    const resultado = view.querySelector('#resultadoSoma');
    expect(resultado.textContent).toBe(resultadoEsperado);
  });
});
