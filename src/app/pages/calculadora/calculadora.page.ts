import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CalculadoraService } from './calculadora.service';

@Component({
  selector: 'app-calculadora',
  templateUrl: './calculadora.page.html',
  styleUrls: ['./calculadora.page.scss'],
})
export class CalculadoraPage implements OnInit {
  form: FormGroup;
  resultado = '0';

  constructor(
    private fb: FormBuilder,
    private calculadoraService: CalculadoraService
  ) {}

  ngOnInit() {
    this.createForm();
  }

  createForm(): void {
    this.form = this.fb.group({
      dividendo: ['', [Validators.required]],
      divisor: ['', [Validators.required]],
    });
  }

  public dividir(): void {
    const { dividendo, divisor } = this.form.value;

    this.resultado = this.calculadoraService.dividir(dividendo, divisor);
  }
}
