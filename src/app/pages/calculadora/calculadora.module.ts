import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CalculadoraPageRoutingModule } from './calculadora-routing.module';
import { CalculadoraPage } from './calculadora.page';
import { OperacaoModule } from './componentes/operacao.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    CalculadoraPageRoutingModule,
    OperacaoModule,
  ],
  declarations: [CalculadoraPage],
})
export class CalculadoraPageModule {}
