import { TestBed } from '@angular/core/testing';

import { CalculadoraService } from './calculadora.service';

describe('CalculadoraService', () => {
  let service: CalculadoraService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CalculadoraService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('deve garantir que 1 + 4 = 5', () => {
    const soma = service.somar(1, 4);
    expect(soma).toEqual('5');
  });
  it('deve garantir que 1 - 4 = -3', () => {
    const soma = service.subtrair(1, 4);
    expect(soma).toEqual('-3');
  });
  it('deve garantir que 1 * 4 = 4', () => {
    const soma = service.multiplicar(1, 4);
    expect(soma).toEqual('4');
  });

  it('deve garantir que 1 / 4 = 0.25', () => {
    const soma = service.dividir(1, 4);
    expect(soma).toEqual('0.25');
  });
});
