import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CalculadoraService {
  constructor() {}

  public dividir(dividendo: number, divisor: number): string {
    if (divisor === 0) {
      return 'Não é possível dividir por 0';
    }
    return (dividendo / divisor).toString();
  }

  public somar(numero1: number, numero2: number): string {
    return (numero1 + numero2).toString();
  }

  public subtrair(numero1: number, numero2: number): string {
    return (numero1 - numero2).toString();
  }

  public multiplicar(numero1: number, numero2: number): string {
    return (numero1 * numero2).toString();
  }
}
