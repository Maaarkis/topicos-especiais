import { Component } from '@angular/core';
import { AuthService } from 'src/app/shared/auth.service';
import { ContasService } from '../contas/services/contas.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  conta = {
    pagar: { valorTotal: 0, totalConta: 0 },
    receber: { valorTotal: 0, totalConta: 0 },
    saldo: { valorTotal: 0, totalConta: 0 },
  };

  dataDePesquisa = new Date().toISOString();

  constructor(
    private authService: AuthService,
    private contaService: ContasService
  ) {}

  ionViewWillEnter() {
    this.atualizarContas();
  }

  public logout(): void {
    this.authService.logout();
  }

  public atualizarContas(): void {
    console.log(this.dataDePesquisa);

    this.contaService
      .totalPor('pagar', this.dataDePesquisa)
      .subscribe((pagar) => {
        this.conta.pagar = pagar;

        this.contaService
          .totalPor('receber', this.dataDePesquisa)
          .subscribe((receber) => {
            this.conta.receber = receber;
            this.atualizarSaldo();
          });
      });
  }

  private atualizarSaldo(): void {
    this.conta.saldo.totalConta =
      this.conta.pagar.totalConta + this.conta.receber.totalConta;

    this.conta.saldo.valorTotal =
      this.conta.receber.valorTotal - this.conta.pagar.valorTotal;
  }
}
