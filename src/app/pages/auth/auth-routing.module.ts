import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ForgotPage } from './forgot/forgot.page';
import { LoginPageModule } from './login/login.module';
import { LoginPage } from './login/login.page';
import { RegisterPage } from './register/register.page';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: LoginPage },
      { path: 'forgot', component: ForgotPage },
      { path: 'register', component: RegisterPage },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class AuthRoutingModule {}
