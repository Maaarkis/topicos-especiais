import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { LoginPageModule } from './login/login.module';
import { RegisterPageModule } from './register/register.module';
import { ForgotPageModule } from './forgot/forgot.module';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    AuthRoutingModule,
    LoginPageModule,
    ForgotPageModule,
    RegisterPageModule,
  ],
})
export class AuthModule {}
