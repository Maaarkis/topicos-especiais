import { Injectable } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ToastService } from 'src/app/shared/toast.service';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { UserLogin, UserRegister } from './interfaces/user.interface';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private pageHome = '/home';
  private pageSignIn = '/auth';

  constructor(
    private toastService: ToastService,
    private fireAuth: AngularFireAuth,
    private nav: NavController
  ) {}

  public async login(user: UserLogin): Promise<void> {
    const { email, password } = user;
    await this.fireAuth
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        this.toastService.showToast({
          message: 'Usuário logado com sucesso',
          duration: 2000,
          position: 'bottom',
        });
        this.nav.navigateForward(this.pageHome);
      })
      .catch(() => {
        this.toastService.showToast({
          message: 'Dados de acessos incorretos',
          duration: 2000,
          position: 'bottom',
        });
      });
  }

  public forgotPassword(email: string): void {
    this.fireAuth
      .sendPasswordResetEmail(email)
      .then(() => {
        this.nav.navigateForward(this.pageSignIn);
        this.toastService.showToast({
          message: 'Email enviado com sucesso',
          duration: 2000,
          position: 'bottom',
        });
      })
      .catch(() => {
        this.toastService.showToast({
          message: 'Email não encontrado',
          duration: 2000,
          position: 'bottom',
        });
      });
  }

  public async register(user: UserRegister) {
    const { email, password } = user;
    await this.fireAuth
      .createUserWithEmailAndPassword(email, password)
      .then(() => {
        this.toastService.showToast({
          message: 'Usuário criado com sucesso',
          duration: 2000,
          position: 'bottom',
        });
        this.nav.navigateForward(this.pageSignIn);
      });
  }

  public isUserLoggerIn(): void {
    this.fireAuth.authState.subscribe((user) => {
      if (user) {
        this.nav.navigateForward(this.pageHome);
      } else {
        this.nav.navigateForward(this.pageSignIn);
      }
    });
  }

  public logout(): void {
    this.fireAuth.signOut().then(() => this.nav.navigateBack(this.pageSignIn));
  }
}
