export class DateHelper {
  static breakDate(date: string) {
    const splitDate = date.split('T')[0].split('-');

    return {
      ano: parseInt(splitDate[0], 10),
      mes: parseInt(splitDate[1], 10),
      dia: parseInt(splitDate[2], 10),
    };
  }
}
