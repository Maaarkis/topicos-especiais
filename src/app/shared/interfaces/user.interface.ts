interface UserRegister {
  nome: string;
  sobrenome: string;
  email: string;
  password: string;
  confirmPassword: string;
}

interface UserLogin {
  email: string;
  password: string;
}

export { UserRegister, UserLogin };
