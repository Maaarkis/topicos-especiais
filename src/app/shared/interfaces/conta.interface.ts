interface ContaRegister {
  id: string;
  nome: string;
  descricao: string;
  valor: number;
  data: Date;
  tipo: string;
}

interface Conta {
  id: string;
  nome: string;
  descricao: string;
  valor: number;
  data: Date;
  tipo: string;
}

export { Conta, ContaRegister };
