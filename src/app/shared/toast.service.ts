import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class ToastService {
  constructor(private toast: ToastController) {}

  public async showToast(options: {
    message: string;
    duration: number;
    position: 'top' | 'bottom' | 'middle';
  }): Promise<void> {
    const toast = await this.toast.create({
      message: options.message,
      duration: options.duration,
      position: options.position,
    });

    toast.present();
  }
}
