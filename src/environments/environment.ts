// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'controle-test',
    appId: '1:1077908261915:web:9e6d66dece942c11a47cbe',
    storageBucket: 'controle-test.appspot.com',
    apiKey: 'AIzaSyCbLGsUvHWBg7L3NiGanXCGFvI1jUxd6MI',
    authDomain: 'controle-test.firebaseapp.com',
    messagingSenderId: '1077908261915',
    measurementId: 'G-B5Q9E0134J',
  },
  production: false,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
